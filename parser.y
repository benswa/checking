%{
#include "lyutils.h"
#include "astree.h"
#include <cassert>

#define YYDEBUG 1
#define YYERROR_VERBOSE 1
#define YYPRINT yyprint
#define YYMALLOC yycalloc

static void* yycalloc (size_t size);
%}

%debug
%defines
%error-verbose
%token-table
%verbose

// Order of increasing precedence goes from top
// to bottom. Also assigning the right and left
// associativity of the tokens.
%right TOK_IF TOK_ELSE
%right '='
%left  TOK_EQ TOK_NE TOK_LT TOK_LE TOK_GT TOK_GE
%left  '+' '-'
%left  '*' '/' '%'
%right TOK_POS TOK_NEG '!' TOK_NEW TOK_ORD TOK_CHR
%left  TOK_ARRAY TOK_FIELD TOK_FUNCTION
%left  '[' '.'
%nonassoc '('

// Declaration of the tokens used for the parser.
%token TOK_VOID TOK_BOOL TOK_CHAR TOK_INT TOK_STRING
%token TOK_IF TOK_ELSE TOK_WHILE TOK_RETURN TOK_STRUCT
%token TOK_FALSE TOK_TRUE TOK_NULL TOK_NEW TOK_ARRAY
%token TOK_EQ TOK_NE TOK_LT TOK_LE TOK_GT TOK_GE
%token TOK_IDENT TOK_INTCON TOK_CHARCON TOK_STRINGCON

%token TOK_ROOT TOK_DECLID TOK_TYPEID TOK_FIELD TOK_INDEX
%token TOK_POS TOK_NEG TOK_CALL TOK_NEWARRAY TOK_ORD TOK_CHR
%token TOK_NEWSTRING TOK_IFELSE TOK_RETURNVOID TOK_BLOCK
%token TOK_VARDECL TOK_FUNCTION TOK_PARAMLIST TOK_PROTOTYPE

// Starting the state machine at 'start'
%start start

%%

// State machine reference assignment to yyparse_astree.
start         : program            { yyparse_astree = $1; }
              ;
// new_parseroot() is called to form the root node.
program       : program structdef  { $$ = adopt1 ($1, $2); }
              | program function   { $$ = adopt1 ($1, $2); }
              | program statement  { $$ = adopt1 ($1, $2); }
              | program error '}'  { $$ = $1; }
              | program error ';'  { $$ = $1; }
              |                    { $$ = new_parseroot (); }
              ;
// The first parts of structdef must be in multfielddecl
// because we are required to have TOK_STRUCT adopt the
// second and the fourth nodes.
structdef     : multfielddecl '}'
                  {
                    free_ast ($2);
                    $$ = $1;
                  }
              | TOK_STRUCT TOK_IDENT '{' '}'
                 {
                   free_ast2 ($3, $4);
                   sym_swap ($2, TOK_TYPEID);
                   $$ = adopt1 ($1, $2);
                 }
              ;
// Represents the first parts of structdef and how to
// handle them. The second statement basically deals
// with the multiple fields that a user can declare
// which if expanded out would simply mean that there
// are a bunch of fields that follow TOK_STRUCT
// TOK_IDENT and '{'.
multfielddecl : TOK_STRUCT TOK_IDENT '{' fielddecl
                  {
                    free_ast ($3);
                    sym_swap ($2, TOK_TYPEID);
                    $$ = adopt2 ($1, $2, $4);
                  }
              | multfielddecl fielddecl
                  {
                    $$ = adopt1 ($1, $2);
                  }
              ;
// Represents the declarations of the fields in the
//   struct. One case deals with declaring arrays and
//   the other case deals with regular variables. The
//   current specs do not allow assignment of values
//   to the fields.
fielddecl     : basetype TOK_ARRAY TOK_IDENT ';'
                  {
                    free_ast ($4);
                    sym_swap ($3, TOK_FIELD);
                    $$ = adopt2 ($2, $1, $3);
                  }
              | basetype TOK_IDENT ';'
                  {
                    free_ast ($3);
                    sym_swap ($2, TOK_FIELD);
                    $$ = adopt1 ($1, $2);
                  }
              ;
// Simply when the type of token's value type is found,
//   it is simply returned as is.
basetype      : TOK_VOID      { $$ = $1; }
              | TOK_BOOL      { $$ = $1; }
              | TOK_CHAR      { $$ = $1; }
              | TOK_INT       { $$ = $1; }
              | TOK_STRING    { $$ = $1; }
              | TOK_TYPEID    { $$ = $1; }
              | TOK_IDENT     { $$ = $1; }
              ;
// The first is the identifier declaration and the second
// is what handles the various numbers of parameters the
// function could have followed by the block at the end.
// A new node is created to take in all the various types
// of parameters and identifier information.
function      : identdecl multidentdecl ')' block
                  {
                    free_ast ($3);
                    $$ = adopt3 (new_parsenode (TOK_FUNCTION,
                                 $1->filenr, $1->linenr,
                                 $1->offset, ""), $1, $2, $4);
                  }
              | identdecl '(' ')' block
                  {
                    free_ast ($3);
                    $$ = adopt3 (new_parsenode (TOK_FUNCTION,
                                 $1->filenr, $1->linenr,
                                 $1->offset, ""), $1, $2, $4);
                  }
              | identdecl multidentdecl ')' ';'
                  {
                    free_ast2 ($3, $4);
                    $$ = adopt2 (new_parsenode (TOK_PROTOTYPE,
                                 $1->filenr, $1->linenr,
                                 $1->offset, ""), $1, $2);
                  }
              | identdecl '(' ')' ';'
                  {
                    free_ast2 ($3, $4);
                    $$ = adopt2 (new_parsenode (TOK_PROTOTYPE,
                                 $1->filenr, $1->linenr,
                                 $1->offset, ""), $1, $2);
                  }
              ;
// Similar to structdef where the second statement is expanded
// out as many times as needed to represent the different number
// of parameters that a function may have.
multidentdecl : '(' identdecl
                {
                  sym_swap ($1, TOK_PARAMLIST);
                  $$ = adopt1 ($1, $2);
                }
              | multidentdecl ',' identdecl
                  {
                    free_ast ($2);
                    $$ = adopt1 ($1, $3);
                  }
              ;
identdecl     : basetype TOK_ARRAY TOK_IDENT
                  {
                    sym_swap ($3, TOK_DECLID);
                    $$ = adopt2 ($2, $1, $3);
                  }
              | basetype TOK_IDENT
                  {
                    sym_swap ($2, TOK_DECLID);
                    $$ = adopt1 ($1, $2);
                  }
              ;
// blockstmnt handle the numerous amount of statements
// possible within the block.
block         : blockstmnt '}'
                  {
                    free_ast ($2);
                    $$ = $1;
                  }
              | '{' '}'
                  { 
                    free_ast($2);
                    sym_swap($1, TOK_BLOCK);
                    $$ = $1;
                  }
              ;
// Similar to structdef where the second statement can
// handle the numerous amount of statements that might 
// be there.
blockstmnt    : '{' statement
                  {
                    $$ = adopt1sym ($1, $2, TOK_BLOCK);
                  }
              | blockstmnt statement
                  {
                    $$ = adopt1 ($1, $2);
                  }
              ;
// Statement straight up returns whatever it gets from
// the various types of code.
statement     : block    { $$ = $1; }
              | vardecl  { $$ = $1; }
              | while    { $$ = $1; }
              | ifelse   { $$ = $1; }
              | return   { $$ = $1; }
              | expr ';' { 
                           free_ast ($2);
                           $$ = $1;
                         }
              | ';'      { $$ = $1; }
              ;
// Variable declaration.
vardecl       : identdecl '=' expr ';'
                  {
                    free_ast ($4);
                    $$ = adopt2sym ($2, $1, $3, TOK_VARDECL);
                  }
              ;
// Identifier declaration is made here. Pretty much
// in the same way we declare variables.
identdecl     : basetype TOK_ARRAY TOK_DECLID
                  { $$ = adopt2 ($2, $1, $3); }
              | basetype TOK_DECLID
                  { $$ = adopt1 ($1, $2); }
              ;
// Various types of expression and variable tokens are
// handled here.
expr          : BINOP        { $$ = $1; }
              | UNOP         { $$ = $1; }
              | allocator    { $$ = $1; }
              | call         { $$ = $1; }
              | '(' expr ')' {
                               free_ast2($1, $3);
                               $$ = $2;
                             }
              | variable     { $$ = $1; }
              | constant     { $$ = $1; }
              ;
// Manual listing of all the different types of binary
// operators possible/allowed. Note that '-' and '+'
// and TOK_NEG and TOK_POS are not the same thing.
BINOP         : expr '=' expr    { $$ = adopt2 ($2, $1, $3); }
              | expr TOK_EQ expr { $$ = adopt2 ($2, $1, $3); }
              | expr TOK_NE expr { $$ = adopt2 ($2, $1, $3); }
              | expr TOK_LT expr { $$ = adopt2 ($2, $1, $3); }
              | expr TOK_LE expr { $$ = adopt2 ($2, $1, $3); }
              | expr TOK_GT expr { $$ = adopt2 ($2, $1, $3); }
              | expr TOK_GE expr { $$ = adopt2 ($2, $1, $3); }
              | expr '+' expr    { $$ = adopt2 ($2, $1, $3); }
              | expr '-' expr    { $$ = adopt2 ($2, $1, $3); }
              | expr '*' expr    { $$ = adopt2 ($2, $1, $3); }
              | expr '/' expr    { $$ = adopt2 ($2, $1, $3); }
              | expr '%' expr    { $$ = adopt2 ($2, $1, $3); }
              ;
// Manual listing of all the different types of unary
// operators possible/allowed. Note that '-' and '+'
// and TOK_NEG and TOK_POS are not the same thing.
UNOP          : '+' expr         { $$ = adopt1sym ($1, $2, TOK_POS); }
              | '-' expr         { $$ = adopt1sym ($1, $2, TOK_NEG); }
              | '!' expr         { $$ = adopt1 ($1, $2); }
              | TOK_ORD expr     { $$ = adopt1 ($1, $2); }
              | TOK_CHR expr     { $$ = adopt1 ($1, $2); }
              ;
// Handles the case where the user uses 'new' for some dynamic
// memory allocation.
allocator     : TOK_NEW TOK_IDENT '(' ')'
                  {
                    free_ast2 ($3, $4);
                    sym_swap ($2, TOK_TYPEID);
                    $$ = adopt1 ($1, $2);
                  }
              | TOK_NEW TOK_STRING '(' expr ')'
                  {
                    free_ast2 ($3, $5);
                    $$ = adopt1sym ($1, $4, TOK_NEWSTRING);
                  }
              | TOK_NEW basetype '[' expr ']'
                  {
                    free_ast2 ($3, $5);
                    $$ = adopt2sym ($1, $2, $4, TOK_NEWARRAY);
                  }
              ;
// Handles function calls with callparam doing the handling of
// numerous number of expressions similar to structdef.
call          : callparam ')'
                  {
                    free_ast ($2);
                    $$ = $1;
                  }
              ;
// Handles the varying number of parameters for a call
// function. Handles zero or more parameters.
callparam     : TOK_IDENT '('
                  {
                    $$ = adopt1sym ($2, $1, TOK_CALL);
                  }
              | TOK_IDENT '(' expr
                  {
                    $$ = adopt2sym ($2, $1, $3, TOK_CALL);
                  }
              | callparam ',' expr
                  {
                    free_ast ($2);
                    $$ = adopt1 ($1, $3);
                  }
              ;
// Handles variables and the types allowed. Handles regular
// variables, arrays, and object variables.
variable      : TOK_IDENT
                  {
                    $$ = $1;
                  }
              | expr '[' expr ']'
                  {
                    free_ast ($4);
                    $$ = adopt2sym ($2, $1, $3, TOK_INDEX);
                  }
              | expr '.' TOK_IDENT
                  {
                    sym_swap ($3, TOK_FIELD);
                    $$ = adopt2 ($2, $1, $3);
                  }
              ;
// Constant tokens are returned as is without modification.
constant      : TOK_INTCON    { $$ = $1; }
              | TOK_CHARCON   { $$ = $1; }
              | TOK_STRINGCON { $$ = $1; }
              | TOK_FALSE     { $$ = $1; }
              | TOK_TRUE      { $$ = $1; }
              | TOK_NULL      { $$ = $1; }
              ;
// Handles while loops.
while         : TOK_WHILE '(' expr ')' statement
                  {
                    free_ast2 ($2, $4);
                    $$ = adopt2 ($1, $3, $5);
                  }
              ;
// Straight forward handling of if and else statements.
ifelse        : TOK_IF '(' expr ')' statement %prec TOK_IF
                  {
                    free_ast2 ($2, $4);
                    $$ = adopt2 ($1, $3, $5);
                  }
              | TOK_IF '(' expr ')' statement %prec TOK_ELSE
                TOK_ELSE statement
                  {
                    free_ast2 ($2, $4);
                    free_ast ($6);
                    sym_swap ($1, TOK_IFELSE);
                    adopt2 ($1, $3, $5);
                    $$ = adopt1 ($1, $7);
                  }
              ;
// Returns are dealt with either not returning anything or
// it will return something.
return        : TOK_RETURN expr ';'
                  {
                    free_ast ($3);
                    $$ = adopt1 ($1, $2);
                  }
              | TOK_RETURN ';'
                  {
                    free_ast ($2);
                    sym_swap ($1, TOK_RETURNVOID);
                    $$ = $1;
                  }
              ;

%%

const char* get_yytname (int symbol) {
   return yytname [YYTRANSLATE (symbol)];
}

bool is_defined_token (int symbol) {
   return YYTRANSLATE (symbol) > YYUNDEFTOK;
}

static void* yycalloc (size_t size) {
   void* result = calloc (1, size);
   assert (result != NULL);
   return result;
}
