#include <cstring>
#include "symboltable.h"
#include "lyutils.h"
#include "auxlib.h"
#include "lyutils.h"

// Symbol stack that tracks all symbols seen.
vector<symbol_table*> perm_sym_stack;

// Symbol stack that tracks blocks necessary for type checking.
vector<symbol_table*> block_count_stack;

// Global counter representing next block number to be used.
// Tracks the number of blocks on the block_count_stack
int next_block = 1;

// Keeps track of the 'actual' block depth visual wise, used for
// printing the else statement
int block_depth = 0;

const string attr_string [ATTR_bitset_size] {
     "void", "bool", "char", "int",
     "null", "string", "struct",
     "array", "function", "variable",
     "field", "typeid", "param",
     "lval", "const", "vreg", "vaddr"
};

int get_attr_bit_index (int symbol) {
  // If the symbol does not match any of the cases, it will return -1
  int attribute = -1;
  
  switch (symbol) {
    case TOK_VOID:
      attribute = ATTR_void;
      break;
    case TOK_BOOL:
      attribute = ATTR_bool;
      break;
    case TOK_CHAR:
      attribute = ATTR_char;
      break;
    case TOK_INT:
      attribute = ATTR_int;
      break;
    case TOK_STRING:
      attribute = ATTR_string;
      break;
    case TOK_STRUCT:
      attribute = ATTR_struct;
      break;
    case TOK_ARRAY:
      attribute = ATTR_array;
      break;
    case TOK_FUNCTION:
      attribute = ATTR_function;
      break;
    case TOK_PROTOTYPE:
      attribute = ATTR_function;
      break;
    case TOK_IDENT:
      attribute = ATTR_variable;
      break;
    case TOK_DECLID:
      attribute = ATTR_variable;
      break;
    case TOK_FIELD:
      attribute = ATTR_field;
      break;
    case TOK_TYPEID:
      attribute = ATTR_typeid;
      break;
    case TOK_PARAMLIST:
      attribute = ATTR_param;
      break;
    case TOK_VARDECL:
      attribute = ATTR_lval;
      break;
    case '=':
      attribute = ATTR_lval;
      break;
    case TOK_INTCON:
      attribute = ATTR_const;
      break;
    case TOK_CHARCON:
      attribute = ATTR_const;
      break;
    case TOK_STRINGCON:
      attribute = ATTR_const;
      break;
    case TOK_FALSE:
      attribute = ATTR_const;
      break;
    case TOK_TRUE:
      attribute = ATTR_const;
      break;
    case TOK_NULL:
      attribute = ATTR_const;
      break;
    case '*' :
      attribute = ATTR_vreg;
      break;
    case '/' :
      attribute = ATTR_vreg;
      break;
    case '+' :
      attribute = ATTR_vreg;
      break;
    case '-' :
      attribute = ATTR_vreg;
      break;
    case '%' :
      attribute = ATTR_vreg;
      break;
    case TOK_EQ :
      attribute = ATTR_vreg;
      break;
    case TOK_NE :
      attribute = ATTR_vreg;
      break;
    case TOK_LT :
      attribute = ATTR_vreg;
      break;
    case TOK_LE :
      attribute = ATTR_vreg;
      break;
    case TOK_GT :
      attribute = ATTR_vreg;
      break;
    case TOK_GE :
      attribute = ATTR_vreg;
      break;
    default :
      // Encoding All unused and unknown token symbols with the last
      // attribute since it seems it is only used for the size of
      // the bitset.
      //cout << "Unidentified: " << get_yytname (symbol) << endl;
      break;
  }
  return attribute;
}

string bitset_to_str (attr_bitset attributes) {
  // Takes the node's attr_bitset value and matches it
  // to the attributes.
  int bit_index;
  string attribute_string = "";
  for (bit_index = 0; bit_index < ATTR_bitset_size; ++bit_index) {
    int bitcheck = attributes.test(bit_index);
    if (bitcheck == 1) {
      attribute_string.append(attr_string[bit_index]);
      attribute_string.append(" ");
    }
  }
  return attribute_string;
}

void set_attribute (astree* node) {
  // This must be called for all functions that access a node
  // No children has ever had its attribute applied so if a
  // function ever creates a symbol, neither the astree or
  // symbol that inherits the astree attribute will have anything

  int attr_bit_index = get_attr_bit_index (node->symbol);

  // Uses get_attr_bit_index() to get attribute code bits
  // Will not apply attribute if the number is outside the ranges
  if ((attr_bit_index <= -1) || (attr_bit_index > ATTR_bitset_size)) {
    return;
  }
  
  // Do not want non-primitive type nodes to be have the
  // variable attribute, but attribute typeid
  if ((node->symbol == TOK_IDENT) &&
     (attr_bit_index == ATTR_variable)) {
    node->attributes.set (ATTR_typeid);
    return;
  }

  node->attributes.set (attr_bit_index);
}

void force_set_attribute (astree* node, int attr_bit_index) {
  // Works similar to set_attribute except it does not
  // automatically discover the attribute and apply it.
  // You must give it an attribute to apply.

  // Will not apply attribute if the number is outside the ranges
  if ((attr_bit_index <= -1) || (attr_bit_index > ATTR_bitset_size)) {
    return;
  }

  node->attributes.set (attr_bit_index);
}

void set_entry_attribute (symbol_entry* sym_entry, int attr_bit_index) {
  // Will not apply attribute if the number is outside the ranges
  
  if ((attr_bit_index <= -1) || (attr_bit_index > ATTR_bitset_size)) {
    return;
  }

  if ((sym_entry->second)->attributes.test(ATTR_field) && 
     (attr_bit_index == ATTR_variable)){
    // If the symbol entry has the attribute field and we are trying
    // to assign 'variable' attribute to the symbol as well
    // Used as edge case in the struct fields
    return;
  }
  
  if ((sym_entry->second)->attributes.test(ATTR_field) && 
     (attr_bit_index == ATTR_typeid)){
    // If the symbol entry has the attribute field and we are trying
    // to assign 'typeid' attribute to the symbol as well
    // Used as edge case in the struct fields
    return;
  }
  
  if ((sym_entry->second)->attributes.test(ATTR_variable) && 
     (attr_bit_index == ATTR_typeid)){
    // If the symbol entry has the attribute variable and we are
    // trying to assign 'typeid' attribute to the symbol as well
    // Used as edge case for adding attributes to symbols in vardecl
    return;
  }

  (sym_entry->second)->attributes.set (attr_bit_index);
}

void set_sym_attribute (symbol* new_symbol, int attr_bit_index) {
  // Will not apply attribute if the number is outside the ranges
  
  if ((attr_bit_index <= -1) || (attr_bit_index > ATTR_bitset_size)) {
    return;
  }
  
  if ((new_symbol->attributes).test(ATTR_variable) && 
     (attr_bit_index == ATTR_typeid)){
    // If the symbol entry has the attribute variable and we are
    // trying to assign typeid to the symbol as well
    // Used as edge case in the function parameters
    return;
  }

  (new_symbol->attributes).set (attr_bit_index);
}

symbol* new_sym (astree* node) {
  symbol* new_symbol = new symbol();
  
  new_symbol->attributes = node->attributes;
  new_symbol->fields = NULL;
  new_symbol->filenr = node->filenr;
  new_symbol->linenr = node->linenr;
  new_symbol->offset = node->offset;
  new_symbol->blocknr = node->blocknr;
  //new_symbol->parameters = NULL; // Field is no longer a pointer
  
  new_symbol->nonprimitive_typeid = "";
  new_symbol->nonprimitive_typeid_address = NULL;
  new_symbol->var_name_id = "";
  
  return new_symbol;
}

string dump_sym_attr(string key, attr_bitset sym_attr) {
  int bit_index;
  string attribute_string = "";
  for (bit_index = 0; bit_index < ATTR_bitset_size; ++bit_index) {
    int bitcheck = sym_attr.test(bit_index);
    if (bitcheck == 1) {
      if (sym_attr.test(ATTR_struct) == 1) {
        // Specifically for structs where the type is the key
        attribute_string.append("struct \"");
        attribute_string.append(key);
        attribute_string.append("\"");
      }
      else {
        attribute_string.append(attr_string[bit_index]);
      }
    attribute_string.append(" ");
    }
  }

  return attribute_string;
}

symbol_entry new_symbol_entry (astree* node, string* key) {
  // Creation of new nodes depending on type of information
  symbol* new_symbol = new_sym (node);      //potential memory leak

  // Using type_id to be the 'key' to the symbol entry
  symbol_entry new_entry = make_pair(key, new_symbol);
  return new_entry;
}

void insert_entry (symbol_table* table, astree* node, string* key) {
  // Constructs a new symbol entry and inserts it into the table.
  symbol_entry new_entry = new_symbol_entry (node, key);
  table->insert(new_entry);
}

symbol_table* new_symbol_table (astree* node, string* key) {
  // Does something extra besides just making new table by adding
  // an entry into the table as well
  symbol_table* new_table = new symbol_table ();//potential memory leak
  insert_entry (new_table, node, key);

  return new_table;
}

void apply_attr_blocknr (astree* node) {
  // Traversing through all the nodes to apply the attribute
  // and blocknr
  if (node == NULL) {
    return;
  }
  
  set_attribute (node);
  node->blocknr = next_block - 1;
  
  for (size_t i = 0; i < (node->children).size(); ++i) {
    apply_attr_blocknr (node->children[i]);
  }
}

int check_nullptr () {
  // Checks if the top of both stacks have nullptr
  // Return 1 for true and 0 for false
  if ((perm_sym_stack.back() == nullptr) && 
     (block_count_stack.back() == nullptr)) {
    return 1;
  }
  else {
    return 0;
  }
}

int check_block_zero (vector<symbol_table*> stack, string* key) {
  // Check to see if this function already exists in the symbol table
  // at block 0 layer of the stack.
  symbol_table::const_iterator result = (*stack[0]).find (key);

  if (result == (*stack[0]).end()) {
    // Return False
    return 0;
  }
  else {
    // Return True
    return 1;
  }
}

void add_table_to_stack () {
  // Will not insert block into the vector if there
  // aren't variables to process. Require both the perm_sym_stack
  // and the block_coun_stack to be nullptr at the top of stack
  if (check_nullptr () == 1) {
    symbol_table* new_table = new symbol_table();//memory leak?
    perm_sym_stack.back() = new_table;
    block_count_stack.back() = new_table;
  }
  else {
    errprintf ("Error: Attempted to push a new block table to ");
    errprintf ("stack when one of the stacks do not have ");
    errprintf ("nullptr at the top on stack.\n");
  }
}

void enter_block () {
  // Pushes a nullptr onto both the local and global stack
  // global stack is never popped
  perm_sym_stack.push_back (nullptr);
  block_count_stack.push_back (nullptr);

  // Increments next_block count
  next_block++;
}

void leave_block () {
  // Won't insert block into the vector if there
  // aren't variables to process.
  
  // Need to investigate whether to go through the symbol table and
  // free up allocated resources to prevent memory leak if necessary.
  
  block_count_stack.pop_back ();
  
  next_block--;
}

void add_field (astree* node, symbol_table* field_table,
                string* struct_typeid) {
  // Assumes there are no assigns allowed in the fields when
  // declaring the struct, so it is safe to hard code this part

  // type_id for the struct is located on its first child
  string* key = (string*)(node->children[0])->lexinfo;

  // applying the attributes to the nodes
  set_attribute (node);
  set_attribute (node->children[0]);
  
  // Setting the block number for the nodes
  node->blocknr = next_block - 1;
  (node->children[0])->blocknr = next_block - 1;
  
  // New entry using the node with the variable name
  symbol_entry new_entry = new_symbol_entry (node->children[0], key);

  for (int bit_index = 0; bit_index < ATTR_bitset_size; ++bit_index) {
    // Since I used the 'TOK_FIELD' node to make the new entry,
    // I need to set the data type to the symbol
    int bitcheck = (node->attributes).test(bit_index);
    if (bitcheck == 1) {
      set_entry_attribute (&new_entry, bit_index);
    }
  }

  // Inserting the field entry into the struct field table
  field_table->insert(new_entry);

  if (node->symbol == TOK_IDENT) {
    // set the nonprimitive_typeid in this field's entry
    // for type checking and printing use
    (*field_table).at(key)->nonprimitive_typeid = *(node->lexinfo);
    (*field_table).at(key)->nonprimitive_typeid_address 
                                             = (string*)(node->lexinfo);
  }
  
  // Printing out the symbol information;
  for (int i = 0; i < block_depth; ++i) {
    fprintf (symFile, "  ");
  }
  
  fprintf (symFile, "%s (%lu.%lu.%lu) {%s} %s",
      (*key).c_str(), (new_entry.second)->filenr,
      (new_entry.second)->linenr,
      (new_entry.second)->offset, (*struct_typeid).c_str(),
      dump_sym_attr(*key, (new_entry.second)->attributes).c_str());

  // Printing Remaining Attribute information
  if ((*field_table).at(key)->nonprimitive_typeid != "") {
    // If the field is a non-primitive type
    fprintf (symFile, "struct \"%s\"\n",
        ((*field_table).at(key)->nonprimitive_typeid).c_str());
  }
  else {
    fprintf (symFile, "\n");
  }
}

void add_struct (astree* node) {
  // type_id for the struct is located on its first child
  string* key = (string*)(node->children[0])->lexinfo; //i.e type_id
  
  // Will not allow the struct to be nested in other 
  // functions and structs
  if ((next_block - 1) >= 1) {
    errprintf ("Error: Cannot declare struct: %s", (*key).c_str());
    errprintf (" within functions or other structs\n");
    return;
  }
  
  // applying the type_id attribute to the first child of struct
  set_attribute (node->children[0]);

  // Setting the block number for the nodes
  node->blocknr = next_block - 1;
  (node->children[0])->blocknr = next_block - 1;
  
  // Putting all structs into block zero layer
  symbol_entry new_entry = new_symbol_entry (node, key);
  (block_count_stack[next_block-1])->insert(new_entry);
  (perm_sym_stack[next_block-1])->insert(new_entry);
  
  // Printing out the symbol information
  fprintf (symFile, "%s (%lu.%lu.%lu) {%lu} %s\n",
      (*key).c_str(), node->filenr, node->linenr,
      node->offset, node->blocknr,
      dump_sym_attr(*key, node->attributes).c_str());
  
  if (node->children.size() > 1) {
    // Sets the block depth for printing purposes
    block_depth = 1;
    
    // Recursively printing the info of the node's children.
    symbol_table* field_table = new symbol_table();// potent mem leak?

    (*block_count_stack[next_block-1]).at(key)->fields = field_table;
    (*perm_sym_stack[next_block-1]).at(key)->fields = field_table;

    for (size_t child = 1; child < node->children.size(); ++child) {
      add_field (node->children[child], field_table, key);
    }
    
    // Resets the block depth after traversing all fields
    block_depth = 0;
  }
  // Once all the struct information is printed, we print a new line
  fprintf (symFile, "\n");
}

void add_paramlist (astree* node, vector<symbol*>* param_vector) {
  // Function must take into account of various number of parameters
  // Must call traverse_atree_rec() if there is a block

  // using function name as the key for the symbol table
  string* param_name = (string*)(node->children[0])->lexinfo;

  // Setting the attributes before making a new symbol entry
  set_attribute (node->children[0]);
  set_attribute (node);

  // Setting the block number for the nodes
  node->blocknr = next_block;
  (node->children[0])->blocknr = next_block;
  
  // Creation of new struct to put into the vector
  // Warning: there is no way to retrieve variable name in vectors
  symbol* new_symbol = new_sym (node->children[0]);  //memory leak?

  for (int bit_index = 0; bit_index < ATTR_bitset_size; ++bit_index) {
    // Since I used the 'TOK_DECLID' node to make the new symbol,
    // I need to set the data type to the symbol
    int bitcheck = (node->attributes).test(bit_index);
    if (bitcheck == 1) {
      set_sym_attribute (new_symbol, bit_index);
    }
  }
  
  // Set the other two parameters for the parameter symbols,
  // not applied to the astree nodes
  set_sym_attribute (new_symbol, ATTR_lval);
  set_sym_attribute (new_symbol, ATTR_param);

  if (node->symbol == TOK_IDENT) {
    // set the nonprimitive_typeid in this field's entry
    // for type checking and printing use
    new_symbol->nonprimitive_typeid = *(node->lexinfo);
    new_symbol->nonprimitive_typeid_address = (string*)(node->lexinfo);
  }
  
  // parameter variable name
  new_symbol->var_name_id = *param_name;
  
  // Pushing the symbol into the function's parameter vector
  param_vector->push_back(new_symbol);
}

void add_function (astree* node) {
  // Function takes into account of various number of parameters.
  // Takes into account if the prototype has already been declared.
  // Able to print the function information even if it does not add new
  // symbol_entry to block 0.
  
  // using function name as the key for the symbol table
  string* key = (string*)(node->children[0]->children[0])->lexinfo;

  if (check_block_zero(perm_sym_stack, key) == 0) {
    // Will not allow the struct to be nested in other functions
    // and structs
    if ((next_block - 1) >= 1) {
      errprintf ("Error: Declaring the function: \"%s\"",
                 (*key).c_str());
      errprintf (" within a function or a struct ");
      errprintf ("at Line %lu, Column %lu.\n", (*key).c_str(),
                 ((node->children[0])->children[0])->linenr,
                 ((node->children[0])->children[0])->offset);
      return;
    }
    
    // Sets the attributes for the typeid node and the name node
    set_attribute (node);
    set_attribute (node->children[0]);
    set_attribute (node->children[0]->children[0]);
    
    
    // Setting the block number for the nodes
    node->blocknr = next_block - 1;
    (node->children[0])->blocknr = next_block - 1;
    ((node->children[0])->children[0])->blocknr = next_block - 1;
    
    // Putting all structs into block zero layer
    symbol_entry new_entry = new_symbol_entry (node, key);
    
    if ((node->children[0])->symbol == TOK_IDENT) {
      // set the nonprimitive_typeid as this function's return type
      (new_entry.second)->nonprimitive_typeid = 
                            *((node->children[0])->lexinfo);
      (new_entry.second)->nonprimitive_typeid_address = 
                            (string*)((node->children[0])->lexinfo);
    }
    
    (block_count_stack[next_block-1])->insert(new_entry);
    (perm_sym_stack[next_block-1])->insert(new_entry);
    
    // Handling the parameters of the function
    if (node->children.size() > 2) {
      astree* param_tree = node->children[1];
      
      set_attribute (param_tree);
      
      // Setting the block number for the nodes
      param_tree->blocknr = next_block;

      if ((param_tree->children).size() > 0) {
        for (size_t child = 0; child < (param_tree->children).size(); 
                                        ++child) {
          // Passing in the param astree and the
          // address of the struct's parameters vector
          add_paramlist (param_tree->children[child],
                    &((new_entry.second)->parameters));
        }
      }
    }
  }
  
  // Setting the blocknr of the block node
  (node->children[2])->blocknr = next_block;
  
  // Printing out the function symbol information
  symbol* current_func = (*block_count_stack[0]).at(key);
  
  fprintf (symFile, "%s (%lu.%lu.%lu) {%lu} ",
      (*key).c_str(), node->filenr, node->linenr,
      node->offset, node->blocknr);
    
  if (current_func->nonprimitive_typeid == "") {
    // If the type of the return function is a not a primitive
    fprintf (symFile, "%s\n",
        dump_sym_attr (*key, node->attributes).c_str());
  }
  else {
    // This is hardcoded to only expect struct as a nonprimitive
    // type solution is to maybe apply the attribute struct to
    // this function and add in the word struct when needed
    
    fprintf (symFile, "struct \"%s\"\n",
        (current_func->nonprimitive_typeid).c_str());
  }
  
  // We increase block depth here for the printing of indentations
  block_depth = 1;
  
  // Printing out the parameter information
  for (size_t child = 0; child < (current_func->parameters).size(); 
       ++child) {
       
    // Tracking each parameter
    symbol* current_param = (current_func->parameters)[child];
    
    // Printing out the indents
    for (int i = 0; i < block_depth; ++i) {
      fprintf (symFile, "  ");
    }
    
    // Printing out the parameter symbol information
    fprintf (symFile, "%s (%lu.%lu.%lu) {%lu} ",
             (current_param->var_name_id).c_str(),
             current_param->filenr, current_param->linenr,
             current_param->offset, current_param->blocknr);
  
    if (current_param->nonprimitive_typeid != "") {
    // This is hardcoded to only expect struct as a nonprimitive
    // type. solution is to maybe apply the attribute struct
    // to this function and add in the word struct when needed
      fprintf (symFile, "struct \"%s\" ",
          (current_param->nonprimitive_typeid).c_str());
    }
  
    fprintf (symFile, "%s\n",
        dump_sym_attr((current_param->var_name_id).c_str(),
                      current_param->attributes).c_str());
  }
  fprintf (symFile, "\n");
  
  // The following code adds the block to the stack
  enter_block ();
  
  if ((node->children).size() == 3) {
    // Checking to make the function node has three children. The
    // check is not really needed since it will always have 3
    // children, prototypes have 2 children though
    
    // astree traversal to go through the function statement block
    for (size_t i = 0; i < ((node->children[2])->children).size();
                                            ++i) {
      traverse_astree_rec ((node->children[2])->children[i]);
    }
  }
  fprintf (symFile, "\n");
  
  // Leaving the block after doing type checking inside it.
  leave_block ();
  
  // We are leaving the function, so depth is zero
  block_depth = 0;
}

void add_vardecl (astree* node) {
  // Variable declarations all share one thing in common in that the
  // left side will always be a TOK_IDENT or some primitive type and
  // the function name.
  
  // using function name as the key for the symbol table
  string* key = (string*)(node->children[0]->children[0])->lexinfo;
  
  // Sets the attributes and blocknr for the nodes
  apply_attr_blocknr (node);

  if (check_nullptr() == 1) {
    // After "entering the block", nullptr is put on top of stack
    // If we try to add an entry to top of stack, we will seg fault
    add_table_to_stack ();
  }
  
  int symbol = node->children[0]->symbol;
  
  switch (symbol) {
    case TOK_INT:
    case TOK_BOOL:
    case TOK_CHAR:
    case TOK_STRING:
      {
      // Creating a new entry using the node with the variable name
      symbol_entry new_entry = 
            new_symbol_entry (node->children[0]->children[0], key);
      
      // Storing variable name in the symbol struct's var_name_id
      (new_entry.second)->var_name_id = *key;
      
      // Checking if the right hand side is a function call
      astree* call_node = node->children[1];
      if (call_node->symbol == TOK_CALL) {
        // Check if function exists and the type
        if (check_block_zero (block_count_stack,
            (string*)(call_node->children[0])->lexinfo) == 0) {
          errprintf ("Calling function: %s has not been declared.",
                     (*(call_node->children[0])->lexinfo).c_str());
          errprintf ("at Line %lu, Column %lu.\n",
               (call_node->children[0])->linenr,
               (call_node->children[0])->offset);
        }
        
        // Check the parameters of the function to see if they match
        if ((call_node->children).size() >= 2) {
          // If there are more than children, there are parameters
          symbol_table::const_iterator sym_entry =
            (*block_count_stack[0]).find((string*)(call_node
              ->children[0])->lexinfo);
          
          // Compare number of params in the stack with the astree node
          // to see if they have the same number of parameters
          if (((sym_entry->second)->parameters).size() !=
              ((call_node->children).size()) - 1) {
            errprintf ("Calling function: %s has wrong amount of ",
                     (*(call_node->children[0])->lexinfo).c_str());
            errprintf ("parameters at Line %lu, Column %lu.\n",
               (call_node->children[0])->linenr,
               (call_node->children[0])->offset);
          }
        }
      }
      
      // Adding the new entry to the table at the top of the stack
      (block_count_stack[next_block-1])->insert(new_entry);
      (perm_sym_stack[next_block-1])->insert(new_entry);
     
      for (int bit_index = 0; bit_index < ATTR_bitset_size;
                                      ++bit_index) {
        // Since I used the 'TOK_DECLID' node to make the new entry,
        // I need to set other attributes
        int bitcheck = (node->attributes).test(bit_index);
        if (bitcheck == 1) {
          // Inheriting the attributes from the TOK_VARDECL node
          set_entry_attribute (&new_entry, bit_index);
        }
        
        bitcheck = (node->children[0]->attributes).test(bit_index);
        if (bitcheck == 1) {
          // Inheriting the type attributes
          set_entry_attribute (&new_entry, bit_index);
        }
      }

      // Printing out the symbol information;
      for (int i = 0; i < block_depth; ++i) {
        fprintf (symFile, "  ");
      }

      fprintf (symFile, "%s (%lu.%lu.%lu) {%lu} ",
            (*key).c_str(), (new_entry.second)->filenr, 
            (new_entry.second)->linenr,
            (new_entry.second)->offset,
            (new_entry.second)->blocknr);
      
      fprintf (symFile, "%s\n",
        dump_sym_attr(*key,(new_entry.second)->attributes).c_str());
      }
      break;
    case TOK_IDENT:
      {
      // Creating a new entry using the node with the variable name
      symbol_entry new_entry = 
            new_symbol_entry (node->children[0]->children[0], key);
      
      // Storing variable name in the symbol struct's var_name_id
      (new_entry.second)->var_name_id = *key;
      
      // Adding non-primitive type to the nonprimitive_typeid string
      (new_entry.second)->nonprimitive_typeid =
                           *((node->children[0])->lexinfo);
      (new_entry.second)->nonprimitive_typeid_address =
                           (string*)((node->children[0])->lexinfo);

      // Adding the new entry to the table at the top of the stack
      (block_count_stack[next_block-1])->insert(new_entry);
      (perm_sym_stack[next_block-1])->insert(new_entry);
     
      for (int bit_index = 0; bit_index < ATTR_bitset_size;
                                      ++bit_index) {
        // Since I used the 'TOK_DECLID' node to make the new entry,
        // I need to set other attributes
        int bitcheck = (node->attributes).test(bit_index);
        if (bitcheck == 1) {
          // Inheriting the attributes from the TOK_VARDECL node
          set_entry_attribute (&new_entry, bit_index);
        }
        
        bitcheck = (node->children[0]->attributes).test(bit_index);
        if (bitcheck == 1) {
          // Inheriting the type attributes
          set_entry_attribute (&new_entry, bit_index);
        }
      }

      // Printing out the symbol information
      for (int i = 0; i < block_depth; ++i) {
        fprintf (symFile, "  ");
      }
      
      fprintf (symFile, "%s (%lu.%lu.%lu) {%lu} ",
            (*key).c_str(), (new_entry.second)->filenr, 
            (new_entry.second)->linenr,
            (new_entry.second)->offset,
            (new_entry.second)->blocknr);

      if ((new_entry.second)->nonprimitive_typeid != "") {
        // This is hardcoded to only expect struct as a nonprimitive
        // type. solution is to maybe apply the attribute struct
        // to this function and add in the word struct when needed
    
        fprintf (symFile, "struct \"%s\" ",
              ((new_entry.second)->nonprimitive_typeid).c_str());
      }
      fprintf (symFile, "%s\n",
        dump_sym_attr(*key,(new_entry.second)->attributes).c_str());
      }
      break;
    default: 
      errprintf ("Uncaught add_vardecl case: %s", get_yytname(symbol));
      errprintf (" in function: %s\n", (*key).c_str());
      return;
  }
  
}

void add_ifelse (astree* node) {
  // The if statement needs to be typechecked and both blocks need to
  // be traversed to be added to the stack
  
  // Sets the attributes for the typeid node and the function name node
  set_attribute (node);
  set_attribute (node->children[0]);
  set_attribute (node->children[0]->children[0]);
  set_attribute (node->children[0]->children[1]);
  
  // Setting the block number for the nodes
  node->blocknr = next_block - 1;
  (node->children[0])->blocknr = next_block - 1;
  (node->children[0]->children[0])->blocknr = next_block - 1;
  (node->children[0]->children[1])->blocknr = next_block - 1;
  (node->children[1])->blocknr = next_block;

  // enter_block() is called to create a nullptr to put on the stack.
  enter_block ();

  // Incrementing the block_depth because we are now in the else
  // statement. It is possible to have nested ifelse statements.
  block_depth++;
  
  // astree traversal here to go through the if statement block
  for (size_t i = 0; i < ((node->children[1])->children).size(); ++i) {
    traverse_astree_rec ((node->children[1])->children[i]);
  }
  
  if (node->children.size() == 3) {
    // Traversing the else statement block if it exist
    
    // enter_block() is called to create a nullptr to put on the stack
    enter_block ();
    
    // astree traversal here to go through the if statement block
    for (size_t i = 0; i < ((node->children[2])->children).size();
                                            ++i) {
      traverse_astree_rec ((node->children[2])->children[i]);
    }
    
    // Leaving the block after doing the type checking inside it.
    leave_block ();
  }
  
  // Decrementing the block_depth after finishing ifelse statements
  block_depth--;
  
  // Leaving the block after doing the type checking inside it.
  leave_block ();
}

int op_eval (astree* node) {
  // Returns 1 if leaf children nodes are TOK_INTCON.
  // Returns 0 if at least one children nodes are not TOK_INTCON.
  
  int op_sym = node->symbol;
  switch (op_sym) {
    case TOK_IDENT:
      {
      string* key = (string*)(node->lexinfo);

      for (size_t i = 0; i < block_count_stack.size(); ++i) {
        // Check to see if this variable already exists
        // at different block layers of the stack.
        symbol_table::const_iterator result =
                                     (*block_count_stack[i]).find (key);
        if (result == (*block_count_stack[i]).end()) {
          // If no luck
          continue;
        }
        else {
          // If a key with that name is found
          if (((result->second->attributes).test(ATTR_variable) == 1) && 
              ((result->second->attributes).test(ATTR_int) == 1) &&
              ((result->second->attributes).test(ATTR_lval) == 1)) {
            return 1;
          }
        }
      }
      return 0;
      }
    case '.':
      {
      string* struct_key = (string*)((node->children[0])->lexinfo);
      string* field_key = (string*)((node->children[1])->lexinfo);
      
      symbol_table::const_iterator result;
      for (size_t i = 0; i < block_count_stack.size(); ++i) {
        // Checks for the new struct declaration
        result = (*block_count_stack[i]).find (struct_key);

        if (*(result->first) != *(struct_key)) {
          // If no luck
          continue;
        }
        else {
          // If key in struct_key variable is found
          string* struct_type_addr =
                 (string*)(result->second)->nonprimitive_typeid_address;   

          // Checking the field type
          symbol_table* field_table = 
                 ((*block_count_stack[0]).at(struct_type_addr)->fields);

          for (auto it = (*field_table).begin();
               it != (*field_table).end();
               ++it) {
            // Iterates through the field_table looking for the field
            if (*(it->first) == *field_key) {
              // Once found, we only need to see if the struct field
              // is of type int and is a field
              if (((it->second)->attributes).test(ATTR_field) &&
                  ((it->second)->attributes).test(ATTR_int)) {
                return 1;
              }
            }
          }
          // If we cannot find the value after iterating through the
          // field table
          errprintf ("Error: Field does not exist: \"%s\" ",
               (*field_key).c_str());
          errprintf ("at Line %lu, Column %lu.\n",
               (node->children[1])->linenr,
               (node->children[1])->offset);
          return 0;
        }
      }
      
      // If We could not find the struct declaration at all
      errprintf ("Error: Struct declaration does not exist: \"%s\" ",
                 (*struct_key).c_str());
      errprintf ("at Line %lu, Column %lu.\n",
                 (node->children[0])->linenr,
                 (node->children[0])->offset);
      return 0;
      }
    case TOK_INTCON:
      // Current node is an integer constant
      return 1;
    case '+':
    case '-':
    case '*':
    case '/':
    case '%':
      // If current node is discovered to be an operator
      if (op_eval (node->children[0]) != 1) {
        return 0;
      }
      if (op_eval (node->children[1]) != 1) {
        return 0;
      }
      return 1;
    default:
      // If the operator does not have two children whose type cannot
      // derive TOK_INTCON then it fails type checking
      errprintf ("Error: Cannot derive integer type because of \"%s\" ",
                 (*(node->lexinfo)).c_str());
      errprintf ("at Line %lu, Column %lu.\n",
                 node->linenr,
                 node->offset);
      return 0;
  }
}

void handle_bool (astree* node) {
  // Traversing through all the nodes to apply the attribute
  // and blocknr
  apply_attr_blocknr (node);
  
  int eq_eval = node->symbol;
  switch (eq_eval) {
    case TOK_EQ:
    case TOK_NE:
    case TOK_LT:
    case TOK_LE:
    case TOK_GT:
    case TOK_GE:
      if (op_eval (node->children[0]) == 0) {
        return;
      }
      if (op_eval (node->children[1]) == 0) {
        return;
      }
      break;
    case TOK_TRUE:
    case TOK_FALSE:
      break;
    default:
      errprintf ("Error: Unhandled equality symbol \"%s\" ",
                 get_yytname(eq_eval));
      errprintf ("at Line %lu, Column %lu.\n",
                 node->linenr, node->offset);
      return;
  }
}

void add_while (astree* node) {
  // The while statement needs to be typechecked and the blocks need to
  // be traversed to be added to the stack

  // Sets the attributes and the blocknr of the while node
  set_attribute (node);
  node->blocknr = next_block - 1;
  
  // Sets the attributes and the blocknr of the block node
  set_attribute (node->children[1]);
  (node->children[1])->blocknr = next_block;
 
  // Handles the equality statement 'branch' of the while astree tree.
  // Type checks as well.
  handle_bool (node->children[0]);
  
  // Incrementing the block_depth because we are about to enter the
  // block and just in case we need to print, we increase block_depth
  block_depth++;
  
  // The following code adds the block to the stack
  enter_block ();
  
  // Handles the block statement 'branch' of the while astree tree
  for (size_t i = 0; i < ((node->children[1])->children).size(); ++i) {
    traverse_astree_rec ((node->children[1])->children[i]);
  }
  fprintf (symFile, "\n");
  
  // Leaving the block after doing type checking inside it.
  leave_block ();
  
  // We are leaving the function, so depth is zero
  block_depth = 0;
}

void handle_assignments (astree* node) {
  // Traversing through all the nodes to apply the attribute
  // and blocknr
  apply_attr_blocknr (node);
  
  int left_expression = (node->children[0])->symbol;
  switch (left_expression) {
    case TOK_IDENT:
    case TOK_INT:
    case TOK_CHAR:
    case TOK_STRING:
    case TOK_BOOL:
      
      break;
    case '.':
      {
      string* struct_key = (string*)((node->children[0])
                                     ->children[0])->lexinfo;
      string* field_key = (string*)((node->children[0])
                                    ->children[1])->lexinfo;
      
      symbol_table::const_iterator result;
      string* struct_type_addr = NULL;
      for (size_t i = 0; i < block_count_stack.size(); ++i) {
        // Checks for the new struct declaration
        result = (*block_count_stack[i]).find (struct_key);
        
        if (result->first != struct_key) {
          // If no luck
          continue;
        }
        else {
          // If key in struct_key variable is found
          struct_type_addr =
                 (string*)(result->second)->nonprimitive_typeid_address;
        }
      }
      
      if (struct_type_addr == NULL) {
        errprintf ("Struct declaration does not exist: ");
        errprintf ("\"%s\" ", (*(((node->children[0])
                                ->children[0])->lexinfo)).c_str());
        errprintf ("at Line %lu, Column %lu.\n",
                    ((node->children[0])->children[0])->linenr,
                    ((node->children[0])->children[0])->offset);
        return;
      }
      
      // Checking the field type
      symbol_table* field_table = 
              ((*block_count_stack[0]).at(struct_type_addr)->fields);
      for (auto it = (*field_table).begin();
            it != (*field_table).end();
            ++it) {
        // Iterates through the field_table looking for the field
        if (*(it->first) == *field_key) {
          // Once found, we only need to see if the struct field
          // is of type int and is a field
          if (((it->second)->attributes).test(ATTR_field) == 0) {
            errprintf ("Field does not exist: ");
            errprintf ("\"%s\" ", (*(((node->children[0])
                                ->children[1])->lexinfo)).c_str());
            errprintf ("at Line %lu, Column %lu.\n",
                        ((node->children[0])->children[1])->linenr,
                        ((node->children[0])->children[1])->offset);
          }
        }
      }
      break;
      }
    default:
      errprintf ("handle_assignments(): Unaccounted expr type ");
      errprintf ("\"%s\" ", (*((node->children[0])->lexinfo)).c_str());
      errprintf ("at Line %lu, Column %lu.\n",
                 (node->children[0])->linenr,
                 (node->children[0])->offset);
      return;
  }
  
  int right_expression = (node->children[1])->symbol;
  switch (right_expression) {
    // No child nodes
    case TOK_IDENT:
    case TOK_CHARCON:
    case TOK_INTCON:
    case TOK_STRINGCON:
    case TOK_FALSE:
    case TOK_TRUE:
    case TOK_NULL:
      
      break;
      
    // One child node
    case TOK_NEW:
    case TOK_CALL:

      break;
      
    // Two child nodes
    case '.':
      
      break;
    case '*' :
    case '/' :
    case '+' :
    case '-' :
    case '%' :
      // Checks the right and left children to make sure they are
      // of type int. Do not care about what opt_eval returns here
      op_eval (node->children[0]);
      op_eval (node->children[1]);
      break;

    default:
      errprintf ("handle_assignments(): Unaccounted expr type ");
      errprintf ("\"%s\" ", (*((node->children[1])->lexinfo)).c_str());
      errprintf ("at Line %lu, Column %lu.\n",
                 (node->children[1])->linenr,
                 (node->children[1])->offset);
      return;
  }
}

void add_prototype (astree* node) {
  // using function name as the key for the symbol table
  string* key = (string*)((node->children[0])->children[0])->lexinfo;

  if (check_block_zero(perm_sym_stack, key) == 1) {
    // If a function or struct with the same name as this function
    // is already on the stack
    errprintf ("Error: Multiple declarations of the function ");
    errprintf ("\"%s\" at Line %lu, Column %lu.\n", (*key).c_str(),
               ((node->children[0])->children[0])->linenr,
               ((node->children[0])->children[0])->offset);
    return;
  }

  if ((next_block - 1) >= 1) {
    // Will not allow the prototype to be nested in other functions
    // and structs
    errprintf ("Error: Declaring the prototype: \"%s\"",
               (*key).c_str());
    errprintf (" within a function or a struct ");
    errprintf ("at Line %lu, Column %lu.\n", (*key).c_str(),
               ((node->children[0])->children[0])->linenr,
               ((node->children[0])->children[0])->offset);
    return;
  }

  // Sets the attributes for the typeid node and the function name node
  set_attribute (node);
  set_attribute (node->children[0]);
  set_attribute ((node->children[0])->children[0]);
  
  // Setting the block number for the nodes
  node->blocknr = next_block - 1;
  (node->children[0])->blocknr = next_block - 1;
  ((node->children[0])->children[0])->blocknr = next_block - 1;

  // Putting all structs into block zero layer
  symbol_entry new_entry = new_symbol_entry (node, key);
  
  if ((node->children[0])->symbol == TOK_IDENT) {
    // set the nonprimitive_typeid as this function's return type
    (new_entry.second)->nonprimitive_typeid = 
                          *((node->children[0])->lexinfo);
    (new_entry.second)->nonprimitive_typeid_address = 
                          (string*)((node->children[0])->lexinfo);
  }
 
  (block_count_stack[next_block-1])->insert(new_entry);
  (perm_sym_stack[next_block-1])->insert(new_entry);
  
  // Handling the parameters of the function
  if (node->children.size() > 1) {
    astree* param_tree = node->children[1];
    
    set_attribute (param_tree);
    
    // Setting the block number for the nodes
    param_tree->blocknr = next_block;

    if ((param_tree->children).size() > 0) {
      for (size_t child = 0; child < (param_tree->children).size(); 
                                      ++child) {
        // Passing in the param astree and the
        // address of the struct's parameters vector
        add_paramlist (param_tree->children[child],
                  &((new_entry.second)->parameters));
      }
    }
  }
  
  // Printing out the function symbol information
  symbol* current_func = (*block_count_stack[0]).at(key);
  
  fprintf (symFile, "%s (%lu.%lu.%lu) {%lu} ",
      (*key).c_str(), node->filenr, node->linenr,
      node->offset, node->blocknr);
    
  if (current_func->nonprimitive_typeid == "") {
    // If the type of the return function is a not a primitive
    fprintf (symFile, "%s\n",
        dump_sym_attr (*key, node->attributes).c_str());
  }
  else {
    // This is hardcoded to only expect struct as a nonprimitive
    // type solution is to maybe apply the attribute struct to
    // this function and add in the word struct when needed
    
    fprintf (symFile, "struct \"%s\"\n",
        (current_func->nonprimitive_typeid).c_str());
  }
  
  // We increase block depth here for the printing of indentations
  block_depth = 1;
  
  // Printing out the parameter information
  for (size_t child = 0; child < (current_func->parameters).size(); 
       ++child) {
       
    // Tracking each parameter
    symbol* current_param = (current_func->parameters)[child];
    
    // Printing out the indents
    for (int i = 0; i < block_depth; ++i) {
      fprintf (symFile, "  ");
    }
    
    // Printing out the parameter symbol information
    fprintf (symFile, "%s (%lu.%lu.%lu) {%lu} ",
             (current_param->var_name_id).c_str(),
             current_param->filenr, current_param->linenr,
             current_param->offset, current_param->blocknr);
  
    if (current_param->nonprimitive_typeid != "") {
    // This is hardcoded to only expect struct as a nonprimitive
    // type. solution is to maybe apply the attribute struct
    // to this function and add in the word struct when needed
      fprintf (symFile, "struct \"%s\" ",
          (current_param->nonprimitive_typeid).c_str());
    }
  
    fprintf (symFile, "%s\n",
        dump_sym_attr((current_param->var_name_id).c_str(),
                      current_param->attributes).c_str());
  }
  fprintf (symFile, "\n");
  
  // We are leaving the function, so depth is zero
  block_depth = 0;
}

void traverse_astree_rec (astree* node) {
  if (node == NULL) return;

  // Sets the node attributes
  set_attribute (node);

  // The index to the first child to be examined.
  size_t child = 0;

  int symbol = node->symbol;
  
  switch (symbol) {
    case TOK_ROOT:
      // Break out of switch to traverse children nodes
      break;
    case TOK_STRUCT:
      add_struct (node);
      return;
    case TOK_FUNCTION:
      add_function (node);
      return;
    case TOK_VARDECL:
      add_vardecl (node);
      return;
    case TOK_IFELSE:
      add_ifelse (node);
      return;
    case '=':
      handle_assignments (node);
      return;
    case TOK_PROTOTYPE:
      add_prototype (node);
      return;
    case TOK_WHILE:
      add_while (node);
      return;
    default:
      errprintf ("Uncaught traverse_astree_rec case: %s\n",
                 get_yytname(symbol));
      return;
  }
  
  // Recursively printing the info of the node's children.
  for (size_t child_index = child; child_index < node->children.size();
      ++child_index) {
    traverse_astree_rec (node->children[child_index]);
  }
}

void traverse_astree (astree* root) {
  // Only pass in the root of the whole astree into this function.
  
  // Building the initial symbol stack for root level 
  symbol_table* global_layer = new symbol_table ();//memory leak?
  block_count_stack.push_back(global_layer);
  perm_sym_stack.push_back(global_layer);

  traverse_astree_rec (root);
  fflush (NULL);
}
