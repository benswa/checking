#ifndef __ASTREE_H__
#define __ASTREE_H__

#include <string>
#include <vector>
using namespace std;

struct astree;
#include "symboltable.h"

struct astree {
   int symbol;                   // token code
   size_t filenr;                // index into filename stack
   size_t linenr;                // line number from source code
   size_t offset;                // offset of token with current line
   const string* lexinfo;        // pointer to lexical information
   vector<astree*> children;     // children of this n-way node
   
   // New fields added for type checking
   attr_bitset attributes;       // attributes
   size_t blocknr;               // block number
   symbol_entry* sym_entry;      // symbol entry
};

// Function to create a new astree node
astree* new_astree (int symbol, int filenr, int linenr, int offset,
                    const char* lexinfo);

// Append one child to the vector of children.
astree* adopt1 (astree* root, astree* child);

// Append two children to the vector of children.
astree* adopt2 (astree* root, astree* left, astree* right);

// Append three children to the vector of children.
astree* adopt3 (astree* root, astree* left, astree* center,
                astree* right);

// Append one children to the vector of children with
// root changed to a different symbol.
astree* adopt1sym (astree* root, astree* child, int symbol);

// Append two children to the vector of children with
// root changed to a different symbol.
astree* adopt2sym (astree* root, astree* left,
                   astree* right, int symbol);

// Changes the node's symbol information
void sym_swap (astree* node, int symbol);

// Dump an astree to a FILE.
void dump_astree (FILE* outfile, astree* root);

// Custom dump of an astree to a FILE.
void fdump_astree (FILE* outfile, astree* tree);

// Debug print an astree.
void yyprint (FILE* outfile, unsigned short toknum,
              astree* yyvaluep);

// Recursively free an astree.
void free_ast (astree* tree);

// Recursively free two astrees.
void free_ast2 (astree* tree1, astree* tree2);

#endif
