#ifndef __SYMBOLTABLE_H__
#define __SYMBOLTABLE_H__

#include <bitset>
#include <unordered_map>
#include <vector>

using namespace std;

// Extern Variable from Main for file address.
extern FILE* symFile;

enum { ATTR_void, ATTR_bool, ATTR_char, ATTR_int, ATTR_null,
       ATTR_string, ATTR_struct, ATTR_array, ATTR_function,
       ATTR_variable, ATTR_field, ATTR_typeid, ATTR_param,
       ATTR_lval, ATTR_const, ATTR_vreg, ATTR_vaddr,
       ATTR_bitset_size,
};
// Number of bits in the bitset required.
using attr_bitset = bitset<ATTR_bitset_size>;

struct symbol;
// This is the actual symbol table
using symbol_table = unordered_map<string*, symbol*>;

// We use make_pair on symbol_entry
using symbol_entry = symbol_table::value_type;

// Inclusion of astree.h MUST be here because astree.h requires
// symbol_entry and attr_bitset to be known first.
#include "astree.h"

struct symbol {
   attr_bitset attributes;
   symbol_table* fields;
   size_t filenr, linenr, offset;
   size_t blocknr;

   // Changed the parameters type because of this:
   //http://stackoverflow.com/questions/6246793/
   //a-vector-in-a-struct-best-approach-c
   vector<symbol*> parameters;
   
   // For type_id of structs
   string nonprimitive_typeid;
   
   string* nonprimitive_typeid_address;
   
   // For variable name_id in function params
   string var_name_id;
};

// Takes the symbol number and returns a bitset with the
// corresponding bit.
int get_attr_bit_index (int symbol);

// Takes the bitset and returns the string representation
string bitset_to_str (attr_bitset attributes);

// Takes an astree node and uses get_attr_bit_index to set
// 'attributes' in the astree node.
void set_attribute (astree* node, int attr_bit_index);

// Creates a new node with the information inside an astree
symbol* new_sym (astree* node);

// Recursive function that traverse_astree calls after setting
// up the stack initially. Other functions also call this
void traverse_astree_rec (astree* node);

// Traverses the astree
void traverse_astree (astree* root);

#endif
